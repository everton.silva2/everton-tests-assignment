const { generateOptionsElements, handleCalculateMonths, renderHistoric } = require("./utils");

window.onload = function () {
    document.getElementById("btn-calculate").onclick = handleCalculateMonths;
	initializeApp();
}

function initializeApp() {
    generateOptionsElements();
    renderHistoric();
}