const { getAll, save } = require('./service');

const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]

function generateOptionsElements() {
   const select = document.getElementById("birthdayMonth")
   months.forEach((month) =>
		select.insertAdjacentHTML(
			"beforeend",
			` <option value="${ month }">${ month }</option>`
		)
   );
}

function validateInputValue(value) {
   return value && months.includes(value) && value !== "none";
}

function parseMonthToNumber(monthName) {
   return months.indexOf(monthName) + 1; 
}

function calculateMonths(currentMonth, inputValue) {
	return currentMonth > inputValue
		? (12 - currentMonth) + inputValue
		: Math.abs(currentMonth - inputValue)
}

function generateText(monthsQuantity) {
	if(monthsQuantity === 0)
		return `Your birthday actually is in this month.`;
	return `Your birthday is in ${  monthsQuantity } months.`;
}

async function handleCalculateMonths() {
	const inputValue = document.getElementById("birthdayMonth").value;
	const calculation = validateAndCalculate(inputValue);
	const outputTextElement = document.querySelector("#output-text");
	if(calculation === false) {
		outputTextElement.textContent = 'Pick a month first';
		return;
	}
	
	outputTextElement.textContent = generateText(calculation);
	await generateHistAndSave(inputValue, calculation)
	renderHistoric()
}

function validateAndCalculate(inputValue) {
	const validateField = validateInputValue(inputValue);
	if(!validateField) {
		return false;
	}
	
	const currentMonth = new Date().getMonth() + 1;
	return calculateMonths(currentMonth, parseMonthToNumber(inputValue));

}

function generateHistAndSave(inputValue, calculation) {
	const historic = {
		createdAt: new Date().toLocaleDateString(),
		birthdayMonth: inputValue,
		result: generateText(calculation)
	};

	return save(historic);
}

async function renderHistoric() {
	const historic = await getAll()
	const tbody = document.querySelector('#tbody');
	tbody.innerHTML = ''
	historic.forEach(entry => {
		tbody.innerHTML += `
			<td>${ entry.createdAt }</td>
			<td>${ entry.birthdayMonth }</td>
			<td>${ entry.result }</td>
		`;
	})
}


module.exports = {
	months,
	handleCalculateMonths,
	validateAndCalculate,
	generateOptionsElements,
	validateInputValue,
	parseMonthToNumber,
	calculateMonths,
	generateText,
	renderHistoric,
	generateHistAndSave
}