const { validateAndCalculate, generateHistAndSave } = require('../utils');
const { getAll } = require('../service')
const supertest = require('supertest');
const request = supertest('http://localhost:3000') 
const axios = require('axios')

jest.mock('axios')

let mockData = {
    createdAt: new Date().toLocaleDateString(),
    birthdayMonth: "May",
    result: "Your birthday is in 8 months."
}

afterEach(() => {
    jest.clearAllMocks()
})

describe("Integration narrow/broad", function() {
    // Narrow
    it("Should validate and calculate", function() {
        const calculation = validateAndCalculate("August")
        expect(calculation).toBe(11)
    })

    it("Should not calculate a invalid month", function() {
        const calculations = validateAndCalculate("A")
        expect(calculations).toBeFalsy()
    })

    it("Should generate and save history", async function() {
        axios.post.mockResolvedValueOnce({ id: 5 })
        const response = await generateHistAndSave("August", 10)

        expect(axios.post).toBeCalled()
        expect(axios.post).toHaveBeenCalledWith('http://localhost:3000', {
            createdAt: new Date().toLocaleDateString(),
            birthdayMonth: "August",
            result: "Your birthday is in 10 months."
        })
        expect(response).toEqual({ id: 5 })
    })

    it("Should return history list", async function() {
        const history = {
            data: [
                mockData,
                {
                    createdAt: new Date().toLocaleDateString(),
                    birthdayMonth: "June",
                    result: "Your birthday is in 9 months."
                }
            ]
        }

        axios.get.mockResolvedValueOnce(history)
        const result = await getAll()

        expect(axios.get).toHaveBeenCalledWith('http://localhost:3000')
        expect(result).toEqual(history.data)
    })

    it("Should return empty array when GET fails", async function() {
        const message = "Network Error"
        axios.get.mockRejectedValueOnce(new Error(message))

        const result = await getAll()

        expect(axios.get).toHaveBeenCalledWith('http://localhost:3000');
        expect(result).toEqual([])
    })

    // Broad
    it("Should call the API and insert the data", async function() {
        const call = await request.post('/')
            .send(mockData)
            .expect(200)

        expect(call.body).toBeDefined()
        expect(call.body).toBeInstanceOf(Object)
        expect(call.body.id).toBeDefined()
        mockData.id = call.body.id
    })

    it("Should call the API and return the data by id", async function() {
        const call = await request.get('/' + mockData.id)
        expect(call.body).toBeDefined()
        expect(call.statusCode).toBe(200)
        expect(call.body).toEqual(mockData)
    })

    it("Should call the API and remove the inserted data", async function() {
        const call = await request.delete('/' + mockData.id)
        expect(call.statusCode).toBe(200)
        expect(call.body).toBeDefined()
    })
})