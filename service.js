const axios = require("axios")

const getAll = async () => {
    try {
        const response = await axios.get('http://localhost:3000')
        return response.data
    } catch(err) {
        return []
    }
}

const save = async (data) => {
    try {
        const response = await axios.post('http://localhost:3000', data)
        return response
    } catch(err) {
        throw new Error('Error on save data', data)
    }
}

const getById = async (id) => {
    const response = await axios.get(`http://localhost:3000/${ id }`)
    return response.data
}

module.exports = {
    getAll,
    save,
    getById
}