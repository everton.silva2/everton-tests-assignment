describe("Month calculator flow", function() {
    it('Should select a month and get the output text and generate history', function () {
        cy.visit('./index.html');
        cy.get('#birthdayMonth').select('January');
        cy.contains('Calculate').click();
        cy.get('#output-text').should('have.text', 'Your birthday is in 4 months.');
        cy.get('#tbody tr').last().get('td').last().should('have.text', 'Your birthday is in 4 months.')
    })
    
    it('Should display a error message when the input was not selected', function () {
        cy.visit('./index.html');
        cy.contains('Calculate').click();
        cy.get('#output-text').should('have.text', 'Pick a month first');
    })
})