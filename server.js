const express = require('express')
const cors = require('cors')
const fs = require('fs')

const app = express()

app.use(cors())

app.use(express.json())

app.listen(3000)

const getFromFile = () => {
    return JSON.parse(fs.readFileSync('db.json'))
}

const writeToFile = (data) => {
    fs.writeFileSync('db.json', JSON.stringify(data, null, 2), 'utf8')
}

if(!fs.existsSync('db.json')) {
    writeToFile('')
}

app.get('/', (req, res) => {
    res.status(200).send(getFromFile())
})

app.get('/:id', (req, res) => {
    const data = getFromFile()
    const record = data.find(({ id }) => id == req.params.id)

    if(!record) {
        return res.status(404).json({
            message: 'Data not found'
        })
    }

    return res.status(200).json(record)
})

app.delete('/:id', (req, res) => {
    const data = getFromFile()
    const index = data.findIndex(({ id }) => id == req.params.id)
    if(index !== -1) {
        data.splice(index, 1)
    }
    writeToFile(data)
    return res.sendStatus(200)
})

app.post('/', (req, res) => {
    let data = getFromFile()

    if(!Array.isArray(data)) {
        data = []
    }

    const id = (data[data.length - 1]?.id || 0)  + 1

    data.push(Object.assign(req. body, { id }))

    writeToFile(data)

    res.status(200).json({
        id
    })
})
