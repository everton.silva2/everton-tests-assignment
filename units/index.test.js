const { validateInputValue, parseMonthToNumber, generateText, calculateMonths } = require('../utils');

describe("Month calculator functionalities", function() {
    it("Should return false when input is not selected", function() {
        const validate = validateInputValue('none');
        expect(validate).toBe(false)
    })

    it("Should return false when input is not a valid month", function() {
        const validate = validateInputValue('T');
        expect(validate).toBe(false)
    })

    it("Should transform the name of a month into its respective number", function() {
        const monthNumber = parseMonthToNumber("September");
        expect(monthNumber).toBe(9);
    })

    it("Should calculate the months quantity", function() {
        const monthsQuantity = calculateMonths(11, 9)
        expect(monthsQuantity).toBe(10)
    })

    it("Should generate the text about the month quantity", function() {
        const output = generateText(11);
        expect(output).toBe("Your birthday is in 11 months.")
    })
})
