## Start the server

Execute `npm run server`
## Run units tests

Execute `npm run test:unit"`

## Run integrations tests

Execute `npm run test:integration`

## Run acceptance tests

Execute `npm run test:acceptance`
